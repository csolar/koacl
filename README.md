# KoACL

KoACL is a very simple implementation of a Role-based Access Control List (ACL), in the sense that permissions are defined for groups rather than individually so that a role effectively represents a group with some set of permissions.

This module is useful when you have multiple user roles defined for your application and you want to specify which role is permitted to perform which action.

## Installing the KoACL module

### Using Git

Go into the `modules` directory of your Kohana application, then run the following command:

	git clone https://bitbucket.org/csolar/koacl.git koacl

### Downloading the source

1. Dowload the latest compressed release (you can see a list of the releases by clicking on the Tags tab) from 

		https://bitbucket.org/csolar/koacl/downloads

2. Un-compress the downloaded source into the `modules` directory of your Kohana application.
3. Re-name the un-compressed directory to `koacl`.

## Getting started

In order to use KoACL, all the relevant modules must be enabled in the modules configuration section within `application/bootstrap.php`:

	Kohana::modules(array(
		...
		'auth'  => MODPATH.'auth',
		'orm'   => MODPATH.'orm',
		'koacl' => MODPATH.'koacl'
		...
	));

[!!] The ORM and Auth modules are required for KoACL to work. 

## Using KoACL

1. Configure the Auth module to use the ORM driver.
2. Enter the roles you need for your application into the database (refer to the ORM module's documentation).
3. Override KoACL default configuration in `modules/koacl/config/koacl.php` by creating a `koacl.php` file in the `application/config` directory. You can specify the following properties:

	* `default_role`: `string`. This is the default role for all users before they get assigned a role explicitly by your application's logic. Generally this is the role with least permissions, only allowing access to the public part of the webapp.
	* `cache`:  `boolean`. It tells KoACL whether to use Kohana's cache module to erm.. cache the access list once it is computed the first time.
	* `role_hierarchy`: `array`. This array enables you to specify relationship between roles, specifically, inheritance. For example you could specify:

			return array(
					...
					'role_hierarchy' array(
					 	'guest'  		=> null,
					 	'user' 			=> 'guest',
					 	'staff'			=> 'user',
					 	'webadmin' 		=> null,
					 	'superadmin'    => 'webadmin'
					 	)
					 );

		what the above is saying is that __guest__ is a _root_ role, __user__ inherits all of __guest__'s permissions adding a few of its own and __staff__ inherits all of __user__'s permission adding its own. This hierarchy is _one-way_ so that __guest__ should never be able to perform an action that is __staff__ enabled.   

4. Specify the access list. First you extend the `Controller_KoACL` controller, then you use the special `@acl` _annotation_ to attach permissions to each `action`:

		class Controller_Blog extends Controller_KoACL
		{
			/**
	         * @acl staff
	         */
	         public function postArticle()
	         {
				// do something here that only staff should be allowed to perform	
			 }
		}

	A comma-separated list of roles can be passed to the `acl` annotation.

	[!!] KoACL is very defensive: if you have inherited your controller from `Controller_KoACL` and you forget to annotate one or more of its actions, KoACL will throw a `KoACL_Exception` when that action is being requested. Thus you __must__ annotate every `action` of a KoACL controller.

You can control what happens when an unauthorized/unauthenticated user tries to access an acl-secured action by overriding the `unauthorized()` and `unauthenticated()` functions respectively in your controller:  

	class Controller_Blog extends Controller_KoACL
	{
		/**
         * @acl staff
         */
         public function postArticle()
         {
			// do something here that only staff should be allowed to perform	
		 }

		 protected function unauthorized()
		 {
		 	//redirect to a user-friendly "forbidden" page
		 }

		 protected function unauthenticated()
		 {
		 	//redirect to the login page here
		 }
	}

By default both functions throw KoACL exceptions.

## Exception Handling

KoACL throws three types of exceptions: `KoACL_Exception`, `KoACL_UnauthenticatedUserException` and `KoACL_UnauthorizedUserException`.

They are all basically `HTTP_Exception`s, as such, you can override their default rendering behaviour. 

Suppose you want to render a special, friendly page whenever a user tries to perform an action that is forbidden for her role, then, you could override KoACL `UnauthorizedUserException.php` file with your own in `application/classes/KoACL/UnauthorizedUserException.php` and override its `get_response` function:

	class KoACL_UnauthenticatedUserException extends KoACL_Exception 
	{
		public function get_response()
		{
			$view = View::factory('errors/unauthenticated');
			$view->body = 'You must be authenticated to access this page dude!!';
			$response = Response::factory()->status(500)
	                                       ->body($view->render());
			return $response;                                           
		}
	}