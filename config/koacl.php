<?php defined('SYSPATH') OR die('No direct access allowed.');

switch(Kohana::$environment)
{
	case Kohana::DEVELOPMENT:
	case Kohana::TESTING:
	case Kohana::STAGING:
		return array(
			'default_role'   => 'guest',
			'cache'          => false,
			
			// 'role_hierarchy' => array(
			// 	'guest'  		=> null,
			// 	'user' 			=> 'guest',
			// 	'staff'			=> 'user',
			// 	'webadmin' 		=> null,
			// 	'superadmin'    => 'webadmin'
			// 	)
			);
	case Kohana::PRODUCTION:
		return array(
			'default_role' => 'guest',
			'cache'        => true,

			// 'role_hierarchy' => array(
			// 	'guest'  		=> null,
			// 	'user' 			=> 'guest',
			// 	'staff'			=> 'user',
			// 	'webadmin' 		=> null,
			// 	'superadmin'    => 'webadmin'
			// 	)
			);
}