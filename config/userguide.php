<?php defined('SYSPATH') OR die('No direct access allowed.');

return array
(
    // Leave this alone
    'modules' => array(
 
        // This should be the path to this modules userguide pages, without the 'guide/'. Ex: '/guide/modulename/' would be 'modulename'
        'koacl' => array(
 
            // Whether this modules userguide pages should be shown
            'enabled' => TRUE,
 
            // The name that should show up on the userguide index page
            'name' => 'KoACL',
 
            // A short description of this module, shown on the index page
            'description' => 'Access Control List (ACL) module',
 
            // Copyright message, shown in the footer for this module
            'copyright' => '&copy; 2013–2014 Cristiano Solarino',
        )   
    ),
 
    /*
     * If you use transparent extension outside the Kohana_ namespace,
     * add your class prefix here. Both common Kohana naming conventions are
     * excluded: 
     *   - Modulename extends Modulename_Core
     *   - Foo extends Modulename_Foo
     * 
     * For example, if you use Modulename_<class_name> for your base classes
     * then you would define:
     */
    'transparent_prefixes' => array(
        'Modulename' => TRUE,
    ),

    // Enable the API browser.  TRUE or FALSE
    'api_browser'  => TRUE,
 
    // Enable these packages in the API browser.  TRUE for all packages, or a string of comma seperated packages, using 'None' for a class with no @package
    // Example: 'api_packages' => 'Kohana,Kohana/Database,Kohana/ORM,None',
    'api_packages' => TRUE,
);