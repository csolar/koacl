# Exception Handling

KoACL throws three types of exceptions: `KoACL_Exception`, `KoACL_UnauthenticatedUserException` and `KoACL_UnauthorizedUserException`.

They are all basically `HTTP_Exception`s, as such, you can override their default rendering behaviour. 

Suppose you want to render a special, friendly page whenever a user tries to perform an action that is forbidden for her role, then, you could override KoACL `UnauthorizedUserException.php` file with your own in `application/classes/KoACL/UnauthorizedUserException.php` and override its `get_response` function:

	class KoACL_UnauthenticatedUserException extends KoACL_Exception 
	{
		public function get_response()
		{
			$view = View::factory('errors/unauthenticated');
			$view->body = 'You must be authenticated to access this page dude!!';
			$response = Response::factory()->status(500)
	                                       ->body($view->render());
			return $response;                                           
		}
	}