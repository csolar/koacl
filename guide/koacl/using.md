# Using KoACL

1. Configure the Auth module to use the ORM driver.
2. Enter the roles you need for your application into the database (refer to the ORM module's documentation).
3. Override KoACL default configuration in `modules/koacl/config/koacl.php` by creating a `koacl.php` file in the `application/config` directory. You can specify the following properties:

	* `default_role`: `string`. This is the default role for all users before they get assigned a role explicitly by your application's logic. Generally this is the role with least permissions, only allowing access to the public part of the webapp.
	* `cache`:  `boolean`. It tells KoACL whether to use Kohana's cache module to erm.. cache the access list once it is computed the first time.
	* `role_hierarchy`: `array`. This array enables you to specify relationship between roles, specifically, inheritance. For example you could specify:

			return array(
					...
					'role_hierarchy' array(
					 	'guest'  		=> null,
					 	'user' 			=> 'guest',
					 	'staff'			=> 'user',
					 	'webadmin' 		=> null,
					 	'superadmin'    => 'webadmin'
					 	)
					 );

		what the above is saying is that __guest__ is a _root_ role, __user__ inherits all of __guest__'s permissions adding a few of its own and __staff__ inherits all of __user__'s permission adding its own. This hierarchy is _one-way_ so that __guest__ should never be able to perform an action that is __staff__ enabled.   

4. Specify the access list. First you extend the `Controller_KoACL` controller, then you use the special `@acl` _annotation_ to attach permissions to each `action`:

		class Controller_Blog extends Controller_KoACL
		{
			/**
	         * @acl staff
	         */
	         public function postArticle()
	         {
				// do something here that only staff should be allowed to perform	
			 }
		}

	A comma-separated list of roles can be passed to the `acl` annotation.

	[!!] KoACL is very defensive: if you have inherited your controller from `Controller_KoACL` and you forget to annotate one or more of its actions, KoACL will throw a `KoACL_Exception` when that action is being requested. Thus you __must__ annotate every `action` of a KoACL controller.

You can control what happens when an unauthorized/unauthenticated user tries to access an acl-secured action by overriding the `unauthorized()` and `unauthenticated()` functions respectively in your controller:  

	class Controller_Blog extends Controller_KoACL
	{
		/**
         * @acl staff
         */
         public function postArticle()
         {
			// do something here that only staff should be allowed to perform	
		 }

		 protected function unauthorized()
		 {
		 	//redirect to a user-friendly "forbidden" page
		 }

		 protected function unauthenticated()
		 {
		 	//redirect to the login page here
		 }
	}

By default both function throw KoACL exceptions.