# KoACL

KoACL is a very simple implementation of a Role-based Access Control List (ACL), in the sense that permissions are defined for groups rather than individually so that a role effectively represents a group with some set of permissions.

This module is useful when you have multiple user roles defined for your application and you want to specify which role is permitted to perform which action.

## Getting started

In order to use KoACL, all the relevant modules must be enabled in the modules configuration section within `application/bootstrap.php`:

	Kohana::modules(array(
		...
		'auth'  => MODPATH.'auth',
		'orm'   => MODPATH.'orm',
		'koacl' => MODPATH.'koacl'
		...
	));

[!!] The ORM and Auth modules are required for KoACL to work. 
