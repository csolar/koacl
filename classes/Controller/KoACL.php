<?php defined('SYSPATH') or die('No direct script access.');

class RoleNode
{
	public $_parent = null;
	public $_label;

	public function __construct($label)
	{
		$this->_label = $label;
	}

	public function parent(RoleNode $parent)
	{
		if(!is_null($parent))
		{
			$this->_parent = $parent;
			$parent->_child = $this;
		}
	}
}

/**
 * [Access Control List][ref-acl] (ACL) is a method for specifying 
 * a list of what users have access to which service. This module implements
 * [Role Based Access Control][ref-rbac] whereby each user is assigned one or more roles,
 * each role effectively representing a set of permissions.
 *
 * [ref-acl]: http://en.wikipedia.org/wiki/Access_control_list
 * [ref-rbac]: http://en.wikipedia.org/wiki/Role-based_access_control
 *
 * @package    Kohana/KoACL
 * @author     Cristiano Solarino
 * @copyright  (c) 2013-2014 BrightMinded Ltd
 * @license    http://kohanaframework.org/license
 */
class Controller_KoACL extends Controller
{

	/**
	 * Indicates whether to use the Cache module to cache the Access List
	 * @var boolean
	 */
	protected $_use_cache = false;

	/**
	 * Default role for all users before they get assigned a role explicitly by the application's logic
	 * @var string
	 */
	protected $_default_role = null;

	/**
	 * Stores each role in convenient object form
	 * @var array
	 */
	protected $_node_map = array();

	/**
	 * Constructs a new KoACL Controller. It loads the role hierarchy specified
	 * in the config into an internal tree structure.
	 *
	 * @param Request $request the Kohana request object
	 * @param Response $response the Kohana response object
	 */
	public function __construct(Request $request, Response $response)
	{
		parent::__construct($request, $response);

		$config = Kohana::$config->load('koacl');
		$this->_use_cache = (boolean)$config->get('cache') && class_exists('Cache');
		$this->_default_role = $config->get('default_role');
		$role_hierarchy = $config->get('role_hierarchy');

		if(!is_null($role_hierarchy))
		{
			foreach ($role_hierarchy as $key => $value) 
			{
				$node = new RoleNode($key);
				$this->_node_map[$key] = $node;

				if(!is_null($value))
				{
					if(isset($this->_node_map[$value]))
						$node->parent($this->_node_map[$value]);
				}
			}
		}
	}

	/**
	 * It intercepts every action request and checks whether the role of the user
	 * requesting the action has permission for executing it.
	 *
	 * @throws KoACL_Exception if no access control was specified for the requested action 
	 * @throws KoACL_UnauthorizedUserException if the user's role is not sufficient for accessing the requested action
	 * @throws KoACL_UnauthenticatedUserException if the requested action requires the requestor to be logged in.
	 */
	public function before()
	{
		parent::before();
		
		//grab the user identity
		$user = Auth::instance()->get_user();
		$role_names = array();

		if(!$user)
		{
			$role_names[] = $this->_default_role;
		}
		else
		{
			//if we have a user get her role
			$roles = $user->roles->find_all();

			foreach ($roles as $role) 
				$role_names[] = $role->name;
		}

		//get the action's annotation
		$acl_roles = $this->acl_roles('action_' . $this->request->action());

		if(count($acl_roles) == 0)
		{
			//we act defensively and force the developer to always specify acl roles for an action
			//avoiding cases where a developer forgets to specify acl on an action that should be
			//strongly secured and users with lower permission are allowed to access it.
			throw new KoACL_Exception('undefined access for resource: :controller->:action', array(':controller' => $this->request->controller(), ':action' => $this->request->action()));
		}
		//compare her role with the action's acl role(s)
		foreach ($role_names as $role_name) 
		{
			if(in_array($role_name, $acl_roles))
			{
				//we may proceed with invoking the requested action
				return;
			}
			else
			{
				//let's check if the user role inherits from the acl role attached to the requested action
				if(isset($this->_node_map[$role_name]))
				{
					$node = $this->_node_map[$role_name];
					while(!is_null($node))
					{
						$node = $node->_parent;

						if(!is_null($node) && in_array($node->_label, $acl_roles))
							return;
					}
				}
			}
		}

		if(!$user)
			$this->unauthenticated();
		else
			$this->unauthorized();
	}

	/**
	 * It implements the default behaviour occurring when a user is trying to 
	 * access an action for which being logged-in is necessary. It can be overriden
	 * by the application.
	 *
	 * @throws KoACL_UnauthenticatedUserException
	 */
	protected function unauthenticated()
	{
		throw new KoACL_UnauthenticatedUserException('user needs to be authenticated to perform action :action', array(':action' => $this->request->action()));
	}

	/**
	 * It implements the default behaviour occurring when a user's role is 
	 * insufficient for accessing the requested action. It can be overridden
	 * by the application.
	 *
	 * @throws KoACL_UnauthorizedUserException
	 */
	protected function unauthorized()
	{
		$user = Auth::instance()->get_user();
		throw new KoACL_UnauthorizedUserException('user :user is not authorized to perform action :action', array(':user' => $user->username, ':action' => $this->request->action()));
	}

	/**
	 * It reads the access control definition for the requested action by 
	 * looking for the @acl annotation in the action's code. The @acl annotation takes a comma-separated
	 * list of roles as its body. If caching is enabled the acl definition is
	 * stored in the cache and never computed again for this particular controller's action.
	 *
	 * @param string $action the name of the action to scan for annotations.
	 * @return array roles extracted from the @acl annotation on the requested action
	 * @throws KoACL_Exception if an error occurs during scanning of the annotation
	 */
	private function acl_roles($action)
	{
		if($this->_use_cache)
		{
			$acl_roles_serialised = Cache::instance()->get($this->cache_key($action));
			if(is_string($acl_roles_serialised))
				return explode(',', $acl_roles_serialised);
		}

		$acl_roles = array();

		try 
		{
			$clazz = new ReflectionClass(get_class($this));
			$method = $clazz->getMethod($action);
			$comments = $method->getDocComment();

			$comments = str_replace('/*', '', $comments);
			$comments = str_replace('*/', '', $comments);
			$comments = str_replace('*', '', $comments);
			$parts = explode("\n", $comments);
			
			$pattern = '/@acl(?:\s+([a-zA-Z_,-\s]*))+/';

			$acl_annotations = array();

			foreach ($parts as $part) 
			{
				$matches = array();
				if(preg_match($pattern, $part, $matches))
					$acl_annotations[] = isset($matches[1]) ? trim($matches[1]) : '';
			}

			foreach ($acl_annotations as $acl_annotation) 
			{
				$roles = explode(',', $acl_annotation);

				foreach ($roles as $role) 
					$acl_roles[] = trim($role);
			}
		} 
		catch (Exception $e) 
		{
			throw new KoACL_Exception('unable to process acl annotations: :errors', array(':errors' => $e->getMessage()));
		}

		if($this->_use_cache)
			Cache::instance()->set($this->cache_key($action), implode(',', $acl_roles));

		return $acl_roles;
	}

	/**
	 * It generates the string to be used as key for a given action's
	 * acl definition when caching of the acl is enabled.
	 *
	 * @param string $action the name of the action that the key is to be generated for
	 * @return string a string that can be used as a key when caching acl definitions
	 */
	protected function cache_key($action)
	{
		return 'koacl:' . get_class($this) . ':' . $action;
	}
}