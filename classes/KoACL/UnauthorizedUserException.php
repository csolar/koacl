<?php defined('SYSPATH') or die('No direct script access.');

class KoACL_UnauthorizedUserException extends KoACL_Exception {}