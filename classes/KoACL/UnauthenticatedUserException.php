<?php defined('SYSPATH') or die('No direct script access.');

class KoACL_UnauthenticatedUserException extends KoACL_Exception {}